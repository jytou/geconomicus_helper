package jyt.geconomicus.helper;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;

import javax.imageio.ImageIO;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class TimerFrame extends JFrame
{
	public enum State {INACTIVE, ACTIVE, CLOSED};
	public enum Icon {FULLSCREEN, RESET_TIMER, SET_TIMER, BULB};
	public enum Theme
	{
		WHITE(Color.BLACK, Color.WHITE, "white"), //$NON-NLS-1$
		BLACK(Color.WHITE, Color.BLACK, "black"); //$NON-NLS-1$
		private Color background;
		private Color foreground;
		private String imageSuffix;

		Theme(Color pBackground, Color pForeground, String pImageSuffix)
		{
			background = pBackground;
			foreground = pForeground;
			imageSuffix = pImageSuffix;
		}
	};
	private int mSetSeconds = 5*60;
	private AtomicInteger mSeconds = new AtomicInteger(5*60);
	private AtomicReference<State> mState = new AtomicReference<TimerFrame.State>(State.INACTIVE);
	private long mEndMoment = -1;
	private AtomicBoolean mTimerRunning = new AtomicBoolean(false);
	private Semaphore mActivateSem = new Semaphore(0);
	private HelperUI mHelperUI;
	private TimerPanel mTimerPanel;
	private Theme mTheme = Theme.BLACK;
	private static final int BUTTONS_SPACING = 10;
	private static final Map<Theme, Map<Icon, BufferedImage>> mIcons = new HashMap<>();
    private static Throwable sClassLoadingErrors = null;
	private static Font mDefaultFont = null;
    static
    {
    	try
		{
    		for (Theme theme : Theme.values())
			{
        		final Map<Icon, BufferedImage> iconMap = new HashMap<>();
    			iconMap.put(Icon.FULLSCREEN,  ImageIO.read(TimerFrame.class.getResourceAsStream("/images/fullscreen-" + theme.imageSuffix + ".png"))); //$NON-NLS-1$ //$NON-NLS-2$
    			iconMap.put(Icon.RESET_TIMER, ImageIO.read(TimerFrame.class.getResourceAsStream("/images/resettimer-" + theme.imageSuffix + ".png"))); //$NON-NLS-1$ //$NON-NLS-2$
    			iconMap.put(Icon.SET_TIMER,   ImageIO.read(TimerFrame.class.getResourceAsStream("/images/settimer-" + theme.imageSuffix + ".png"))); //$NON-NLS-1$ //$NON-NLS-2$
    			iconMap.put(Icon.BULB,        ImageIO.read(TimerFrame.class.getResourceAsStream("/images/bulb-" + theme.imageSuffix + ".png"))); //$NON-NLS-1$ //$NON-NLS-2$
    			mIcons.put(theme, iconMap);
			}
			mDefaultFont = Font.createFont(Font.TRUETYPE_FONT, TimerFrame.class.getResourceAsStream("/fonts/latin-modern.otf")); //$NON-NLS-1$
		}
		catch (IOException | FontFormatException e)
		{
			sClassLoadingErrors = e;
		}
    }

	protected static final String TEMPLATE_STRING = "00:00"; //$NON-NLS-1$
	private Font mCurrentFont = mDefaultFont;
	private Point mCurrentPoint = new Point();

	private class TimerPanel extends JPanel
	{
		public TimerPanel()
		{
			super();
			addComponentListener(new ComponentAdapter()
			{
				@Override
				public void componentResized(ComponentEvent e)
				{
					super.componentResized(e);
					chooseFontSize();
					repaint();
				}
			});
		}

		@Override
		protected void paintComponent(Graphics g)
		{
			super.paintComponent(g);
			g.setColor(mTheme.background);
			g.fillRect(0, 0, getWidth(), getHeight());
			g.setColor(mTheme.foreground);
			g.setFont(mDefaultFont.deriveFont(20f));
			g.drawString(getStringForSeconds(mSetSeconds), BUTTONS_SPACING, BUTTONS_SPACING + 20);
			g.setFont(mCurrentFont);
			g.drawString(getStringForSeconds(mSeconds.get()), mCurrentPoint.x, mCurrentPoint.y);
			final BufferedImage icon2FullScreen = mIcons.get(mTheme).get(Icon.FULLSCREEN);
			final BufferedImage iconResetTimer = mIcons.get(mTheme).get(Icon.RESET_TIMER);
			final BufferedImage iconSetTimer = mIcons.get(mTheme).get(Icon.SET_TIMER);
			final BufferedImage iconBulb = mIcons.get(mTheme).get(Icon.BULB);
			g.drawImage(icon2FullScreen, getWidth() - icon2FullScreen.getWidth() - BUTTONS_SPACING, getHeight() - icon2FullScreen.getHeight() - BUTTONS_SPACING, null);
			int x = BUTTONS_SPACING;
			g.drawImage(iconResetTimer, x, getHeight() - icon2FullScreen.getHeight() - BUTTONS_SPACING, null);
			x += iconResetTimer.getWidth() + BUTTONS_SPACING;
			g.drawImage(iconBulb, x, getHeight() - icon2FullScreen.getHeight() - BUTTONS_SPACING, null);
			x += iconBulb.getWidth() + BUTTONS_SPACING;
			if (!mTimerRunning.get())
				g.drawImage(iconSetTimer, x, getHeight() - icon2FullScreen.getHeight() - BUTTONS_SPACING, null);
		}

		private String getStringForSeconds(int pSeconds)
		{
			return String.format("%2d:%02d", pSeconds / 60, pSeconds % 60); //$NON-NLS-1$
		}

		private void chooseFontSize()
		{
			float fontSize = 32;
			float factor;
			boolean testTooLarge;
			if (fits(fontSize, true))
			// go bigger
			{
				factor = 1.1f;
				testTooLarge = true;
			}
			else
			// go smaller
			{
				factor = 0.9f;
				testTooLarge = false;
			}
			while (fits(fontSize, testTooLarge))
				fontSize *= factor;
			fontSize /= factor;
			mCurrentFont = mDefaultFont.deriveFont(fontSize);
			final FontMetrics metrics = getFontMetrics(mDefaultFont.deriveFont(fontSize));
			final int stringWidth = metrics.stringWidth(TEMPLATE_STRING);
			mCurrentPoint = new Point(getWidth() / 2 - stringWidth / 2, getHeight() / 2 + metrics.getHeight() / 4);
		}
	}

	private boolean fits(float pSize, boolean pTestTooLarge)
	{
		final int w = getWidth();
		final int h = getHeight();
		final FontMetrics metrics = getFontMetrics(mDefaultFont.deriveFont(pSize));
		final int stringWidth = metrics.stringWidth(TEMPLATE_STRING);
		if (pTestTooLarge)
		{
			if (stringWidth >= w)
				return false;
			if (metrics.getHeight() >= h)
				return false;
			return true;
		}
		else
		{
			// test too small
			if (stringWidth < w)
				return false;
			if (metrics.getHeight() < h)
				return false;
			return true;
		}
	}

	public class MissignResourceException extends Exception
	{
		public MissignResourceException()
		{
			super("Could not find some resource, exception " + TimerFrame.sClassLoadingErrors.getClass().getCanonicalName() + ", saying " + TimerFrame.sClassLoadingErrors.getMessage());  //$NON-NLS-1$//$NON-NLS-2$
		}
	}

	public TimerFrame(final HelperUI pParent) throws MissignResourceException
	{
		super("Timer"); //$NON-NLS-1$
		if (sClassLoadingErrors != null)
			throw new MissignResourceException();
		getContentPane().setLayout(new BorderLayout());
		getContentPane().add(mTimerPanel = new TimerPanel());
		mHelperUI = pParent;
		addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent pEvent)
			{
				super.windowClosing(pEvent);
				mState.set(State.CLOSED);
				mHelperUI.closedTimerFrame();
			}
		});
		setUndecorated(true);
		toggleFullScreen();
		new Thread("Clock Thread") //$NON-NLS-1$
		{
			public void run()
			{
				try
				{
					while (true)
					{
						mTimerRunning.set(false);
						repaint();
						mActivateSem.acquire();
						mTimerRunning.set(true);
						repaint();
						while (true)
						{
							synchronized (TimerFrame.this)
							{
								if (!TimerFrame.State.ACTIVE.equals(mState.get()))
									break;
							}
							final long endMoment = mEndMoment;
							if (endMoment == -1)
							{
								mSeconds.set(mSetSeconds);
								break;
							}
							else
							{
								final int newValue = (int)(endMoment - System.currentTimeMillis()) / 1000;
								final int oldValue = mSeconds.getAndSet(newValue);
								if (oldValue != newValue)
								{
									if (newValue == 0)
									{
										mState.set(TimerFrame.State.INACTIVE);
										new Thread()
										{
											public void run()
											{
												try
												{
													final Clip clip = AudioSystem.getClip();
													final AudioInputStream inputStream = AudioSystem.getAudioInputStream(TimerFrame.class.getResourceAsStream("/sounds/horn.wav")); //$NON-NLS-1$
													clip.open(inputStream);
													clip.start();
												}
												catch (LineUnavailableException
														| UnsupportedAudioFileException
														| IOException e)
												{
													e.printStackTrace();
												} 
											}
										}.start();
									}
									mTimerPanel.repaint();
//									System.out.println(newValue);
								}
								Thread.sleep(50);
							}
						}
						if (TimerFrame.State.CLOSED.equals(mState))
							break;
					}
				}
				catch (InterruptedException e)
				{
				}
			}
		}.start();
		mTimerPanel.addMouseListener(new MouseAdapter()
		{
			@Override
			public void mouseClicked(MouseEvent e)
			{
				super.mouseClicked(e);
				final BufferedImage icon2FullScreen = mIcons.get(mTheme).get(Icon.FULLSCREEN);
				final BufferedImage iconResetTimer = mIcons.get(mTheme).get(Icon.RESET_TIMER);
				final BufferedImage iconSetTimer = mIcons.get(mTheme).get(Icon.SET_TIMER);
				final BufferedImage iconBulb = mIcons.get(mTheme).get(Icon.BULB);
				if ((e.getX() >= mTimerPanel.getWidth() - icon2FullScreen.getWidth() - BUTTONS_SPACING) &&
				    (e.getX() <= mTimerPanel.getWidth() - BUTTONS_SPACING) &&
				    (e.getY() >= mTimerPanel.getHeight() - icon2FullScreen.getHeight() - BUTTONS_SPACING) &&
				    (e.getY() <= mTimerPanel.getHeight() - BUTTONS_SPACING))
					toggleFullScreen();
				else if ((e.getX() >= BUTTONS_SPACING) &&
				         (e.getX() <= iconResetTimer.getWidth() + BUTTONS_SPACING) &&
				         (e.getY() >= mTimerPanel.getHeight() - iconResetTimer.getHeight() - BUTTONS_SPACING) &&
				         (e.getY() <= mTimerPanel.getHeight() - BUTTONS_SPACING))
					resetTimer();
				else if ((!State.ACTIVE.equals(mState.get())) &&
				         (e.getX() >= iconResetTimer.getWidth() + BUTTONS_SPACING * 2) &&
				         (e.getX() <= iconResetTimer.getWidth() + BUTTONS_SPACING * 2 + iconBulb.getWidth()) &&
				         (e.getY() >= mTimerPanel.getHeight() - iconBulb.getHeight() - BUTTONS_SPACING) &&
				         (e.getY() <= mTimerPanel.getHeight() - BUTTONS_SPACING))
				{
					mTheme = Theme.WHITE.equals(mTheme) ? Theme.BLACK : Theme.WHITE;
					TimerFrame.this.repaint();
				}
				else if ((!State.ACTIVE.equals(mState.get())) &&
				         (e.getX() >= iconResetTimer.getWidth() + iconBulb.getWidth() + BUTTONS_SPACING * 3) &&
				         (e.getX() <= iconResetTimer.getWidth() + iconBulb.getWidth() + BUTTONS_SPACING * 3 + iconSetTimer.getWidth()) &&
				         (e.getY() >= mTimerPanel.getHeight() - iconSetTimer.getHeight() - BUTTONS_SPACING) &&
				         (e.getY() <= mTimerPanel.getHeight() - BUTTONS_SPACING))
				{
					String res = JOptionPane.showInputDialog(TimerFrame.this, UIMessages.getString("TimerFrame.Dialog.MessageNewNumberSeconds"), mSetSeconds); //$NON-NLS-1$
					if (res != null)
					{
						res = res.trim();
						try
						{
							if (res.contains(":")) //$NON-NLS-1$
							{
								final int minutes = Integer.valueOf(res.substring(0, res.indexOf(':')));
								final int seconds = Integer.valueOf(res.substring(res.indexOf(':') + 1));
								setSetSeconds(minutes * 60 + seconds);
							}
							else
							{
								if (res.endsWith("m")) //$NON-NLS-1$
								// the user specified minutes, typed something like "4m"
								{
									res = res.substring(0, res.length() - 1);
									setSetSeconds(Integer.valueOf(res.trim()) * 60);
								}
								else
								{
									if (res.endsWith("s")) //$NON-NLS-1$
									// In case the user typed "300s" or "300 s"
										res = res.substring(0, res.length() - 1);
									setSetSeconds(Integer.valueOf(res.trim()));
								}
							}
						}
						catch (NumberFormatException ex)
						{
							JOptionPane.showMessageDialog(TimerFrame.this, UIMessages.getString("TimerFrame.Error.Message.FormatNewNumberSeconds"), UIMessages.getString("TimerFrame.Error.Title.CouldNotSetTimer"), JOptionPane.ERROR_MESSAGE); //$NON-NLS-1$ //$NON-NLS-2$
						}
					}
				}
				else
					startOrPause();
			}
		});
	}

	protected void toggleFullScreen()
	{
		final GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
		// the screen that we shouldn't use
		final GraphicsConfiguration defaultScreen = ge.getDefaultScreenDevice().getDefaultConfiguration();
		final GraphicsDevice[] allDevices = ge.getScreenDevices();
		GraphicsDevice useDevice = null;
		for (GraphicsDevice graphicsDevice : allDevices)
		{
			if (graphicsDevice.getDefaultConfiguration() != defaultScreen)
			{
				useDevice = graphicsDevice;
				break;
			}
		}
		if (useDevice == null)
			useDevice = defaultScreen.getDevice();
		if (useDevice.getFullScreenWindow() == this)
		{
			// Go to window state
			setVisible(false);
			dispose();
			setUndecorated(false);
			pack();
			useDevice.setFullScreenWindow(null);
			setResizable(true);
			final Rectangle b = useDevice.getDefaultConfiguration().getBounds();
			setBounds(b.x + 10, b.y + 10, b.width - 100, b.height-100);
			setVisible(true);
		}
		else
		{
			// Go to full screen state
			setVisible(false);
			dispose();
			setUndecorated(true);
			pack();
			setResizable(false);
			setBounds(useDevice.getDefaultConfiguration().getBounds());
			useDevice.setFullScreenWindow(this);
			setVisible(true);
		}
	}

	public void startOrPause()
	{
		if (State.ACTIVE.equals(mState.get()))
			pauseTimer();
		else if (State.INACTIVE.equals(mState.get()))
			startTimer();
	}

	private void startTimer()
	{
		if (mSeconds.get() <= 0)
			resetTimer();
		mEndMoment = System.currentTimeMillis() + mSeconds.get() * 1000;
		mState.set(State.ACTIVE);
		mActivateSem.release();
		repaint();
	}

	private void pauseTimer()
	{
		mState.set(State.INACTIVE);
		repaint();
	}

	public void resetTimer()
	{
		mState.set(State.INACTIVE);
		while (mTimerRunning.get())
			try
			{
				Thread.sleep(100);
			}
			catch (InterruptedException e)
			{
				// should never happen
			}
		mSeconds.set(mSetSeconds);
		repaint();
	}

	public void setSetSeconds(int pSetSeconds)
	{
		mSetSeconds = pSetSeconds;
		mSeconds.set(mSetSeconds);
		repaint();
	}
}
