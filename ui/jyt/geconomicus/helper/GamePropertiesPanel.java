package jyt.geconomicus.helper;

import java.awt.Color;
import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.EntityManager;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListCellRenderer;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class GamePropertiesPanel extends JPanel
{
	public boolean mChosen;
	private JComboBox<Integer> mMoneySystemCB;
	private JTextField mDateTextField;
	private JTextField mLocationTextField;
	private JTextField mAnimatorTextField;
	private JTextField mEmailTextField;
	private JTextField mNbTurnsTextField;
	private JTextField mMoneyCardsFactorTextField;
	private JTextField mSmallCoinValueTextField;
	private JLabel mMoneySystemLabel;
	private JLabel mDateLabel;
	private JLabel mLocationLabel;
	private JLabel mAnimatorLabel;
	private JLabel mEmailLabel;
	private JLabel mNbTurnsLabel;
	private JLabel mMoneyCardsFactorLabel;
	private JLabel mSmallCoinValueLabel;
	private JButton mApplyButton;
	public JLabel mErrorGameButtonPanel;
	public EntityManager mEntityManager;
	private JTextArea mDescriptionTextArea;
	private JCheckBox mTake1Penalty;
	private JCheckBox mUseLibreMoney3;
	private boolean mChanged = false;

	public GamePropertiesPanel(final EntityManager pEntityManager, final String pApplyButtonLabel)
	{
		super();
		int y = 0;
		setLayout(new GridBagLayout());
		Insets insets = new Insets(0, 0, 0, 0);
		add(mDateLabel = new JLabel(UIMessageKeyProvider.GAME_DATE_LABEL.getMessage()), new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0));
		mDateTextField = new JTextField(new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z").format(new Date())); //$NON-NLS-1$
		add(mDateTextField, new GridBagConstraints(1, y++, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0, 0));
		add(mMoneySystemLabel = new JLabel(UIMessages.getString("ChooseGameDialog.NewGame.MoneyType.Label")), new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0)); //$NON-NLS-1$
		mMoneySystemCB = new JComboBox<>(new Integer[] {-1, 0, 1});
		mMoneySystemCB.setRenderer(new ListCellRenderer<Integer>()
		{
			@Override
			public Component getListCellRendererComponent(JList<? extends Integer> pList, Integer pValue, int pIndex, boolean pIsSelected, boolean pCellHasFocus)
			{
				String text = ""; //$NON-NLS-1$
				switch (pValue.intValue())
				{
				case -1:// this is empty
					break;
				case Game.MONEY_DEBT:
					text = UIMessageKeyProvider.GENERAL_DEBT_MONEY.getMessage();
					break;
				case Game.MONEY_LIBRE:
					text = UIMessageKeyProvider.GENERAL_LIBRE_CURRENCY.getMessage();
					break;
				default:
					// This shouldn't happen
					System.err.println("Unknown value for money type in combobox"); //$NON-NLS-1$
				}
				return new JLabel(text);
			}
		});
		mMoneySystemCB.requestFocusInWindow();
		add(mMoneySystemCB, new GridBagConstraints(1, y++, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0, 0));
		add(mLocationLabel = new JLabel(UIMessages.getString("ChooseGameDialog.NewGame.Location.Label")), new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0)); //$NON-NLS-1$
		mLocationTextField = new JTextField();
		add(mLocationTextField, new GridBagConstraints(1, y++, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0, 0));
		add(mAnimatorLabel = new JLabel(UIMessages.getString("ChooseGameDialog.NewGame.AnimatorPseudo.Label")), new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0)); //$NON-NLS-1$
		mAnimatorTextField = new JTextField();
		add(mAnimatorTextField, new GridBagConstraints(1, y++, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0, 0));
		add(mEmailLabel = new JLabel(UIMessages.getString("ChooseGameDialog.NewGame.AnimatorEmail.Label")), new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0)); //$NON-NLS-1$
		mEmailTextField = new JTextField();
		add(mEmailTextField, new GridBagConstraints(1, y++, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0, 0));
		add(mNbTurnsLabel = new JLabel(UIMessages.getString("ChooseGameDialog.NewGame.NbTurns.Label")), new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0)); //$NON-NLS-1$
		mNbTurnsTextField = new JTextField(""); //$NON-NLS-1$
		add(mNbTurnsTextField, new GridBagConstraints(1, y++, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0, 0));
		add(mMoneyCardsFactorLabel = new JLabel(UIMessages.getString("ChooseGameDialog.NewGame.CardMoneyFactor.Label")), new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0)); //$NON-NLS-1$
		mMoneyCardsFactorTextField = new JTextField(""); //$NON-NLS-1$
		add(mMoneyCardsFactorTextField, new GridBagConstraints(1, y++, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0, 0));
		add(mSmallCoinValueLabel = new JLabel(UIMessages.getString("ChooseGameDialog.NewGame.SmallCoinValue.Label")), new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0)); //$NON-NLS-1$
		mSmallCoinValueTextField = new JTextField(""); //$NON-NLS-1$
		add(mSmallCoinValueTextField, new GridBagConstraints(1, y++, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0, 0));
		add(new JLabel(UIMessages.getString("ChooseGameDialog.NewGame.Take1Penalty.Label")), new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0)); //$NON-NLS-1$
		mTake1Penalty = new JCheckBox();
		mTake1Penalty.setSelected(true);
		add(mTake1Penalty, new GridBagConstraints(1, y++, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0, 0));
		add(new JLabel(UIMessages.getString("ChooseGameDialog.NewGame.UseLibreMoney3.Label")), new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0)); //$NON-NLS-1$
		mUseLibreMoney3 = new JCheckBox();
		mUseLibreMoney3.setSelected(false);
		add(mUseLibreMoney3, new GridBagConstraints(1, y++, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0, 0));
		add(new JLabel(UIMessageKeyProvider.GAME_DESCRIPTION_LABEL.getMessage()), new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0));
		mDescriptionTextArea = new JTextArea();
		final JScrollPane descriptionScrollPane = new JScrollPane(mDescriptionTextArea);
		add(descriptionScrollPane, new GridBagConstraints(1, y++, 1, 1, 1, 1, GridBagConstraints.WEST, GridBagConstraints.BOTH, insets, 0, 0));

		final JPanel applyButtonPanel = new JPanel(new GridBagLayout());
		mErrorGameButtonPanel = new JLabel();
		mErrorGameButtonPanel.setForeground(Color.red);
		applyButtonPanel.add(mErrorGameButtonPanel, new GridBagConstraints(0, 0, 1, 1, 1, 0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, insets, 0, 0));
		mApplyButton = new JButton(pApplyButtonLabel);
		applyButtonPanel.add(getApplyButton(), new GridBagConstraints(1, 0, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0));

		getApplyButton().setEnabled(false);
		add(applyButtonPanel, new GridBagConstraints(0, y++, 2, 1, 1, 0, GridBagConstraints.CENTER, GridBagConstraints.HORIZONTAL, insets, 0, 0));

		mMoneySystemCB.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent pE)
			{
				activateApplyButton();
				boolean libre = ((Integer)mMoneySystemCB.getSelectedItem()).intValue() == Game.MONEY_LIBRE;
				mTake1Penalty.setEnabled(libre);
				mUseLibreMoney3.setEnabled(libre);
			}
		});
		final DocumentListener buttonActivator = new DocumentListener()
		{
			@Override
			public void removeUpdate(DocumentEvent pEvent)
			{
				activateApplyButton();
			}
			
			@Override
			public void insertUpdate(DocumentEvent pEvent)
			{
				activateApplyButton();
			}
			
			@Override
			public void changedUpdate(DocumentEvent pEvent)
			{
				activateApplyButton();
			}
		};
		mDateTextField.getDocument().addDocumentListener(buttonActivator);
		mLocationTextField.getDocument().addDocumentListener(buttonActivator);
		mAnimatorTextField.getDocument().addDocumentListener(buttonActivator);
		mEmailTextField.getDocument().addDocumentListener(buttonActivator);
		mNbTurnsTextField.getDocument().addDocumentListener(buttonActivator);
		mMoneyCardsFactorTextField.getDocument().addDocumentListener(buttonActivator);
		mSmallCoinValueTextField.getDocument().addDocumentListener(buttonActivator);
		mDescriptionTextArea.getDocument().addDocumentListener(buttonActivator);// this is useful to catch changes
		mTake1Penalty.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				mChanged = true;
			}
		});
		mUseLibreMoney3.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				mChanged = true;
				mMoneyCardsFactorTextField.setEnabled(!mUseLibreMoney3.isSelected());
				mSmallCoinValueTextField.setEnabled(!mUseLibreMoney3.isSelected());
				if (mUseLibreMoney3.isSelected())
				{
					mMoneyCardsFactorTextField.setText("3"); //$NON-NLS-1$
					mSmallCoinValueTextField.setText("1"); //$NON-NLS-1$
				}
			}
		});
	}

	public void setGame(final Game pGame)
	{
		mMoneySystemCB.setSelectedItem(pGame.getMoneySystem());
		mNbTurnsTextField.setText(String.valueOf(pGame.getNbTurnsPlanned()));
		mAnimatorTextField.setText(pGame.getAnimatorPseudo());
		mEmailTextField.setText(pGame.getAnimatorEmail());
		mDescriptionTextArea.setText(pGame.getDescription());
		mDateTextField.setText(pGame.getCurdate());
		mLocationTextField.setText(pGame.getLocation());
		mMoneyCardsFactorTextField.setText(String.valueOf(pGame.getMoneyCardsFactor()));
		mSmallCoinValueTextField.setText(NumberFormat.getInstance().format(pGame.getSmallCoinValue().doubleValue()));
		mTake1Penalty.setSelected(pGame.isTake1Penalty());
		mUseLibreMoney3.setSelected(pGame.isUseStrongUnits3().booleanValue());
		mChanged = false;
	}

	public boolean isChanged()
	{
		return mChanged;
	}

	/**
	 * Activates the new game button and makes sure that the different fields have been correctly filled beforehand.
	 * Shows a relevant error depending on the fields that have been incorrectly filled.
	 */
	private void activateApplyButton()
	{
		mChanged = true;
		String error = null;
		final Color darkGreen = new Color(0, 150, 0);
		mMoneySystemLabel.setForeground(darkGreen);
		mDateLabel.setForeground(darkGreen);;
		mLocationLabel.setForeground(darkGreen);;
		mAnimatorLabel.setForeground(darkGreen);
		mEmailLabel.setForeground(darkGreen);;
		mNbTurnsLabel.setForeground(darkGreen);;
		mMoneyCardsFactorLabel.setForeground(darkGreen);;
		mSmallCoinValueLabel.setForeground(darkGreen);;

		if (mMoneySystemCB.getSelectedIndex() == 0)
		{
			error = UIMessages.getString("ChooseGameDialog.ChooseMonetarySystem.ErrorHint"); //$NON-NLS-1$
			mMoneySystemLabel.setForeground(Color.RED);
		}
		else if (mDateTextField.getText().isEmpty())
		{
			error = UIMessages.getString("ChooseGameDialog.DateCannotBeEmpty.ErrorHint"); //$NON-NLS-1$
			mDateLabel.setForeground(Color.RED);
		}
		else if (mLocationTextField.getText().isEmpty())
		{
			error = UIMessages.getString("ChooseGameDialog.LocationCannotBeEmpty.ErrorHint"); //$NON-NLS-1$
			mLocationLabel.setForeground(Color.RED);
		}
		else if (mAnimatorTextField.getText().isEmpty())
		{
			error = UIMessages.getString("ChooseGameDialog.ChooseAnimator.ErrorHint"); //$NON-NLS-1$
			mAnimatorLabel.setForeground(Color.RED);
		}
		else if (!mEmailTextField.getText().contains("@")) //$NON-NLS-1$
		{
			error = UIMessages.getString("ChooseGameDialog.EmailMustHaveAt.ErrorHint"); //$NON-NLS-1$
			mEmailLabel.setForeground(Color.RED);
		}
		else if (mNbTurnsTextField.getText().isEmpty())
		{
			error = UIMessages.getString("ChooseGameDialog.NbTurnsMustBeFilled.ErrorHint"); //$NON-NLS-1$
			mNbTurnsLabel.setForeground(Color.RED);
		}
		else if (mMoneyCardsFactorTextField.getText().isEmpty())
		{
			error = UIMessages.getString("ChooseGameDialog.FactorMustBeFilled.ErrorHint"); //$NON-NLS-1$
			mMoneyCardsFactorLabel.setForeground(Color.RED);
		}
		else if (mSmallCoinValueTextField.getText().isEmpty())
		{
			error = UIMessages.getString("ChooseGameDialog.SmallCoinValueMustBeFilled.ErrorHint"); //$NON-NLS-1$
			mSmallCoinValueLabel.setForeground(Color.RED);
		}
		if (error == null)
		{
			try
			{
				Integer.parseInt(mNbTurnsTextField.getText());
			}
			catch (NumberFormatException e)
			{
				error = UIMessages.getString("ChooseGameDialog.NbTurnsMustBeInt.ErrorHint"); //$NON-NLS-1$
				mNbTurnsLabel.setForeground(Color.RED);
			}
			try
			{
				Integer.parseInt(mMoneyCardsFactorTextField.getText());
			}
			catch (NumberFormatException e)
			{
				error = UIMessages.getString("ChooseGameDialog.MoneyFactorMustBeInt.ErrorHint"); //$NON-NLS-1$
				mMoneyCardsFactorLabel.setForeground(Color.RED);
			}
			try
			{
				Number d = NumberFormat.getInstance().parse(mSmallCoinValueTextField.getText());
				if ((!NumberFormat.getInstance().format(d.doubleValue()).equals(mSmallCoinValueTextField.getText())) && (!String.valueOf(d.intValue()).equals(mSmallCoinValueTextField.getText())))
				{
					error = UIMessages.getString("ChooseGameDialog.SmallCoinValueMustBeDouble.ErrorBadSeparator"); //$NON-NLS-1$
					mSmallCoinValueLabel.setForeground(Color.RED);
				}
			}
			catch (ParseException e)
			{
				error = UIMessages.getString("ChooseGameDialog.SmallCoinValueMustBeDouble.ErrorHint"); //$NON-NLS-1$
				mSmallCoinValueLabel.setForeground(Color.RED);
			}
			try
			{
				new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z").parse(mDateTextField.getText()); //$NON-NLS-1$
			}
			catch (ParseException e)
			{
				error = UIMessages.getString("ChooseGameDialog.IncorrectDateFormat.ErrorHint"); //$NON-NLS-1$
				mDateLabel.setForeground(Color.RED);
			}
		}
		getApplyButton().setEnabled(error == null);
		mErrorGameButtonPanel.setText(error);
	}

	public JButton getApplyButton()
	{
		return mApplyButton;
	}

	public JComboBox<Integer> getMoneySystemCB()
	{
		return mMoneySystemCB;
	}

	public JTextField getNbTurnsTextField()
	{
		return mNbTurnsTextField;
	}

	public JTextField getAnimatorTextField()
	{
		return mAnimatorTextField;
	}

	public JTextField getEmailTextField()
	{
		return mEmailTextField;
	}

	public JTextField getDateTextField()
	{
		return mDateTextField;
	}

	public JTextField getLocationTextField()
	{
		return mLocationTextField;
	}

	public JTextField getMoneyCardsFactorTextField()
	{
		return mMoneyCardsFactorTextField;
	}

	public JTextField getSmallCoinValueTextField()
	{
		return mSmallCoinValueTextField;
	}

	public JTextArea getDescriptionTextArea()
	{
		return mDescriptionTextArea;
	}

	public JCheckBox getTake1Penalty()
	{
		return mTake1Penalty;
	}

	public JCheckBox getUseLibreMoney3()
	{
		return mUseLibreMoney3;
	}
}