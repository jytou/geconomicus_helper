package jyt.geconomicus.helper.migration;

import javax.persistence.EntityManager;
import javax.swing.JFrame;

import jyt.geconomicus.helper.Event;
import jyt.geconomicus.helper.Game;
import jyt.geconomicus.helper.IEventListener;

public class GameUpgrader
{

	public static boolean upgradeGame(final JFrame pParentComponent, final Game pGame, final EntityManager pEntityManager)
	{
		if ((pGame.getVersion() == null) || (pGame.getSmallCoinValue() == null) || (pGame.getMoneyCardsFactorRaw() == null))
		{
			final MigrateToV100Dialog migrateToV100Dialog = new MigrateToV100Dialog(pParentComponent, pGame, pEntityManager);
			migrateToV100Dialog.setVisible(true);
			if (!migrateToV100Dialog.isCanceled())
			{
				pGame.recomputeAll(new IEventListener()
				{
					@Override
					public void eventApplied(Event pEvent) {}
				});
				pGame.setVersion("1.0.0"); //$NON-NLS-1$
				return true;
			}
			else
				return false;
		}
		else
			return true;
	}

}
