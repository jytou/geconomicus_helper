package jyt.geconomicus.helper.migration;

import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

import javax.persistence.EntityManager;
import javax.swing.AbstractAction;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;

import jyt.geconomicus.helper.Game;
import jyt.geconomicus.helper.UIMessageKeyProvider;
import jyt.geconomicus.helper.UIMessages;

/**
 * A dialog to change the description of the game, if needed.
 * @author jytou
 */
public class MigrateToV100Dialog extends JDialog
{
	private static final String ESCAPE_ACTION = "escape"; //$NON-NLS-1$
	private static final String ENTER_ACTION = "enter"; //$NON-NLS-1$
	private boolean mCanceled = false;

	private class CancelAction extends AbstractAction implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent pE)
		{
			mCanceled = true;
			setVisible(false);
		}
	}

	private class AddAction extends AbstractAction implements ActionListener
	{
		private JTextField mMoneyCardFactorTF;
		private JTextField mSmallCoinValueTF;
		private JCheckBox mTake1Penalty;

		private AddAction(JTextField pMoneyCardFactorTF, JTextField pSmallCoinValueTF, JCheckBox pTake1Penalty)
		{
			mMoneyCardFactorTF = pMoneyCardFactorTF;
			mSmallCoinValueTF = pSmallCoinValueTF;
			mTake1Penalty = pTake1Penalty;
		}

		@Override
		public void actionPerformed(ActionEvent pEvent)
		{
			mGame.setMoneyCardsFactor(Integer.valueOf(mMoneyCardFactorTF.getText()));
			mGame.setSmallCoinValue(Double.valueOf(mSmallCoinValueTF.getText()));
			mGame.setTake1Penalty(mTake1Penalty.isSelected());
			mGame.setVersion("1.0.0"); //$NON-NLS-1$
			setVisible(false);
		}
	}

	private Game mGame;

	public boolean isCanceled()
	{
		return mCanceled;
	}

	public MigrateToV100Dialog(final JFrame pParent, final Game pGame, final EntityManager pEntityManager)
	{
		super(pParent, UIMessageKeyProvider.GAME_DESCRIPTION_LABEL.getMessage());
		mGame = pGame;
		setSize(600, 400);
		setLocation(200, 200);
		setModal(true);
		final JPanel mainPanel = new JPanel(new GridBagLayout());
		final Insets insets = new Insets(4, 4, 4, 4);
		int y = 0;
		mainPanel.add(new JLabel(UIMessageKeyProvider.GAME_DATE_LABEL.getMessage()), new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0));
		mainPanel.add(new JLabel(pGame.getCurdate()), new GridBagConstraints(1, y++, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0, 0));
		mainPanel.add(new JLabel(UIMessages.getString("ChooseGameDialog.NewGame.MoneyType.Label")), new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0)); //$NON-NLS-1$
		mainPanel.add(new JLabel(pGame.getMoneySystem() == Game.MONEY_DEBT ? UIMessageKeyProvider.GENERAL_DEBT_MONEY.getMessage() : UIMessageKeyProvider.GENERAL_LIBRE_CURRENCY.getMessage()), new GridBagConstraints(1, y++, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0, 0));
		mainPanel.add(new JLabel(UIMessages.getString("ChooseGameDialog.NewGame.Location.Label")), new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0)); //$NON-NLS-1$
		mainPanel.add(new JLabel(pGame.getLocation()), new GridBagConstraints(1, y++, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0, 0));
		mainPanel.add(new JLabel(UIMessages.getString("ChooseGameDialog.NewGame.AnimatorPseudo.Label")), new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0)); //$NON-NLS-1$
		mainPanel.add(new JLabel(pGame.getAnimatorPseudo()), new GridBagConstraints(1, y++, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0, 0));
		mainPanel.add(new JLabel(UIMessages.getString("MigrateToV100.Explanation.label")), new GridBagConstraints(0, y++, 2, 1, 0, 1, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0, 0)); //$NON-NLS-1$
		mainPanel.add(new JLabel(UIMessages.getString("ChooseGameDialog.NewGame.CardMoneyFactor.Label")), new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0)); //$NON-NLS-1$
		final JTextField moneyCardFactorTF = new JTextField(mGame.getMoneySystem() == Game.MONEY_DEBT ? mGame.getMoneyCardsFactor() : mGame.getMoneyCardsFactor() * 3);
		mainPanel.add(moneyCardFactorTF, new GridBagConstraints(1, y++, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, insets, 0, 0));
		mainPanel.add(new JLabel(UIMessages.getString("ChooseGameDialog.NewGame.SmallCoinValue.Label")), new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0)); //$NON-NLS-1$
		final JTextField smallCoinValueTF = new JTextField(mGame.getMoneyCardsFactor() == 1 ? "1" : "0.5"); //$NON-NLS-1$ //$NON-NLS-2$
		mainPanel.add(smallCoinValueTF, new GridBagConstraints(1, y++, 1, 1, 1, 1, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, insets, 0, 0));
		final JCheckBox take1Penalty = new JCheckBox();
		take1Penalty.setSelected(true);
		if (pGame.getMoneySystem() == Game.MONEY_LIBRE)
		{
			mainPanel.add(new JLabel(UIMessages.getString("ChooseGameDialog.NewGame.Take1Penalty.Label")), new GridBagConstraints(0, y, 1, 1, 0, 0, GridBagConstraints.EAST, GridBagConstraints.NONE, insets, 0, 0)); //$NON-NLS-1$
			mainPanel.add(take1Penalty, new GridBagConstraints(1, y++, 1, 1, 1, 0, GridBagConstraints.WEST, GridBagConstraints.HORIZONTAL, insets, 0, 0));
		}
		moneyCardFactorTF.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, InputEvent.CTRL_DOWN_MASK), MigrateToV100Dialog.ENTER_ACTION);
		moneyCardFactorTF.getActionMap().put(MigrateToV100Dialog.ENTER_ACTION, new AddAction(moneyCardFactorTF, smallCoinValueTF, take1Penalty));
		moneyCardFactorTF.getInputMap().put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), MigrateToV100Dialog.ESCAPE_ACTION);
		moneyCardFactorTF.getActionMap().put(MigrateToV100Dialog.ESCAPE_ACTION, new CancelAction());
		moneyCardFactorTF.requestFocusInWindow();

		final JPanel buttonsPanel = new JPanel(new FlowLayout());
		final JButton addButton = new JButton(UIMessageKeyProvider.DIALOG_BUTTON_APPLY.getMessage());
		buttonsPanel.add(addButton);
		addButton.addActionListener(new AddAction(moneyCardFactorTF, smallCoinValueTF, take1Penalty));
		final JButton cancelButton = new JButton(UIMessageKeyProvider.DIALOG_BUTTON_CANCEL.getMessage());
		buttonsPanel.add(cancelButton);
		cancelButton.addActionListener(new CancelAction());
		mainPanel.add(buttonsPanel, new GridBagConstraints(0, y, 2, 1, 1, 0, GridBagConstraints.EAST, GridBagConstraints.HORIZONTAL, insets, 0, 0));
		getContentPane().add(mainPanel);
	}
}
