Voici une checklist pour organiser un jeu à **deux organisateurs** ayant chacun leur ordinateur (un animateur et un banquier).

# Monnaie dette

## Démarrage

- **!!! important !!!** avant de démarrer quoi que ce soit, synchronisation des horloges des 2 PC,
- création de la partie sur l'ordinateur de l'animateur, 
- enregistrement des noms des joueurs par l'animateur,
- transfert de son fichier vers le banquier pendant que l'animateur explique le fonctionnement du jeu aux joueurs,
- à quoi correspond une partie => une vie,
- règles d'utilisation des cartes (carrés, valeur des types de cartes, …),
- règles avec le banquier (crédit de 3 => remboursement de 4, taux réaliste, défaut de paiement et prison, etc.)
- bien rappeler que le troc est interdit car ça fausse l'expérience (on veut comparer l'usage de monnaies).

## Inter-tours

- l'animateur annonce le(s) mort(s) de ce tour (il est conseillé de suivre ce que préconise le programme),
- les autres joueurs doivent passer voir le banquier pour rembourser les intérêts et les crédits, et ceux qui veulent peuvent contracter de nouveaux crédits. Pour les crédits, le banquier doit s'assurer que le joueur a bien en main 1,5 fois la valeur du montant demandé et des intérêts. En cours de partie, le banquier peut décider de reserrer ou relâcher ses exigences, c'est lui qui décide !
- avant de relancer un nouveau tour, le banquier s'assure que tous ceux qui avaient des dettes sont passés le voir et donne le feu vert à l'animateur.

Rappel : le prix des cartes est de 1 UNL pour les cartes basses, 2 UNL pour les cartes moyennes et 4 UNL pour les cartes hautes.

## Gestion des morts/renaissances

L'animateur peut décider de :

- gérer les morts pendant le tour suivant pendant que les autres joueurs jouent pour gagner du temps (auquel cas il faudra faire un tour de jeu supplémentaire pour la partie)
- ou bien de les faire passer à l'inter-tour comme les autres joureurs (ça fait perdre un peu de temps d'enregistrement mais c'est plus juste).

Dans tous les cas, les morts doivent passer voir le banquier *avant* l'animateur pour solder leurs dettes, ils passent ensuite voir l'animateur avec ce qui leur reste.
Le banquier note d'abord le remboursement (ou défaut) du crédit, puis note ensuite leur mort dans son fichier (avec un événement « mort ) en comptant la monnaie qui leur reste en main après avoir remboursé leur crédit, cela afin d'avoir une masse monétaire juste en cours de partie. Cet événement « mort » du fichier du banquier ne sera automatiquement pas importé par le programme en fin de partie dans le fichier de l'animateur.

## Gestion des tours

L'animateur chronomètre. Il est déconseillé de faire des crédits en cours de tour, même si ça reste possible.

# Fin de la partie monnaie-dette et préparation de la partie monnaie libre

Le banquier donne son fichier à l'animateur, qui l'importe et vérifie que ses données sont cohérentes.

L'animateur crée une nouvelle partie (cette fois en monnaie libre). Le banquier n'a pas besoin de gérer informatiquement sa partie pour la monnaie libre, puisqu'elle consiste seulement à distribuer le DU.

L'animateur explique le fonctionnement de la monnaie libre :

- en début de partie, chaque jouer se voit distribuer 7 DU ou unités monétaires (on peut commencer avec 2 jetons forts et 3 jetons moyens ou encore 2 forts 2 moyens et 2 faibles),
- distribution 7 unités monétaires à chaque tour : on fait tourner les valeurs des jetons de couleurs,
- il faut aller voir l'animateur à chaque tour,
- celui-ci donne à chaque joueur 2 jetons de la nouvelle valeur forte, il supprime les jetons de la plus petite valeur en donnant une petite compensation (un jeton moyen pour chaque paire de jetons faibles écartée),
- la masse monétaire moyenne est donc de 7 DU par joueur en permanence, avec des unités dont la valeur est divisée par deux à chaque tour,
- bien noter que dans la vraie vie cette complexité est en réalité gérée par le programme Duniter et qu'elle est totalement transparente.

Pour rappel, le prix des cartes est de 3 unités monétaires pour les cartes basses, 6 pour les moyennes et 12 pour les hautes.

Le « banquier » peut préparer les DU à distribuer pendant que les joueurs jouent. Il s'assure avant de démarrer un nouveau tour qu'il a bien récupéré toutes les pièces de faible valeur.

# Fin de la partie en monnaie libre et compte-rendu

Montrer et commenter les graphiques.
Demander surtout les ressentis des joueurs.

